from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError

from app import db
from app.api.comments.model import Comment

comments_blueprint = Blueprint('comments', __name__)


@comments_blueprint.route('/comments/ping', methods=['GET'])
def ping_pong():
    return jsonify({
        'status': 'success',
        'message': 'pong!'
    })


@comments_blueprint.route('/comments', methods=['POST'])
def post_comment():
    post_data = request.get_json()
    response_object = {
        'status': 'fail',
        'message': 'Invalid payload.',
        'data': None
    }
    post_id = post_data.get('post_id')
    body = post_data.get('body')
    if not post_id or not body:
        return jsonify(response_object), 400
    try:
        comment = Comment(post_id=post_id, body=body)
        db.session.add(comment)
        db.session.commit()
        response_object = {
            'status': 'success',
            'data': {
                'post_id': comment.post_id,
                'body': comment.body
            }
        }
        return jsonify(response_object), 200
    except IntegrityError:
        response_object['message'] = 'Post does not exist'
        return jsonify(response_object), 400
