import uuid
from app import db
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID


class Comment(db.Model):
    __tablename__ = "comments"
    id = db.Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    post_id = db.Column(UUID(as_uuid=True), db.ForeignKey('posts.id'),
                        nullable=False)
    body = db.Column(db.String(240), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True,
                           onupdate=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def __init__(self, post_id, body):
        self.post_id = post_id
        self.body = body
