
from app import ma


class CommentSchema(ma.Schema):
    class Meta:
        fields = ('id', 'post_id', 'body', 'created_at',
                  'updated_at', 'deleted_at')


comment_schema = CommentSchema()
comments_schema = CommentSchema(many=True)
