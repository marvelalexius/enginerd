import json
from flask import Blueprint, jsonify, request
from sqlalchemy import exc
from flask_jwt_extended import jwt_required

from app.api.channels.model import Channel
from app.api.channels.schema import channels_schema, channel_schema
from app import db, cache

channels_blueprint = Blueprint('channels', __name__)


@channels_blueprint.route('/channels', methods=['GET'])
def all_channels():
    """Get all channels"""
    page = request.args.get('page', 1, type=int)
    channel_query = Channel.query.paginate(page, 10, True).items
    response_object = {
        'status': 'success',
        'data': {
            'channels': channels_schema.dump(channel_query).data
        }
    }
    return jsonify(response_object), 200


@channels_blueprint.route('/channels', methods=['POST'])
@jwt_required
def create_channel():
    post_data = request.get_json()
    response_object = {
        'status': 'fail',
        'message': 'Invalid payload.',
        'data': None
    }
    if not post_data:
        return jsonify(response_object), 400
    name = post_data.get('name')
    picture = post_data.get('picture')
    desc = post_data.get('desc')
    if not name or not desc or not picture:
        return jsonify(response_object), 400
    try:
        existing_channel = Channel.query.filter_by(name=name).first()
        if not existing_channel:
            channel = Channel(name=name, desc=desc, picture=picture)
            db.session.add(channel)
            db.session.commit()

            response_object['status'] = 'success'
            response_object['message'] = f'{name} was added!'
            response_object['data'] = channel_schema.dump(channel).data
            return jsonify(response_object), 201
        else:
            response_object['message'] = 'Sorry. That name already exists.'
            return jsonify(response_object), 400
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(response_object), 400


@channels_blueprint.route('/channels/<slug>', methods=['GET'])
def show_channel(slug):
    cache_key = 'channel_' + str(slug)
    cache_result = cache.get(cache_key)

    if cache_result:
        default_response = json.loads(cache_result)
        return jsonify(default_response), 200

    """Get single channel detail"""
    default_response = {
        'status': 'fail',
        'message': 'Channel does not exist'
    }

    try:
        channel_query = Channel.query.filter_by(slug=slug).first()
        if not channel_query:
            return jsonify(default_response), 404
        else:
            default_response = {
                'status': 'success',
                'data': channel_schema.dump(channel_query).data
            }

            cache.set(cache_key, json.dumps(default_response))

            return jsonify(default_response), 200
    except ValueError:
        return jsonify(default_response), 404


@channels_blueprint.route('/channels/<slug>', methods=['PUT'])
@jwt_required
def edit_channel(slug):
    """ Update channels """
    json_data = request.get_json()
    default_response = {
        'status': 'fail',
        'message': 'Invalid payload.'
    }

    name = json_data.get('name')
    desc = json_data.get('desc')
    picture = json_data.get('picture')

    if not name or not desc or not picture:
        return jsonify(default_response), 400

    try:
        channel = Channel.query.filter_by(slug=slug).first()
        if not channel:
            default_response['message'] = f'Channel {slug} not found.'
            return jsonify(default_response), 404
        else:
            channel.name = name
            channel.desc = desc
            channel.picture = picture

            db.session.commit()

            cache_key = 'show_channel_' + slug
            cache.delete(cache_key)

        default_response['status'] = 'success'
        default_response['message'] = f'{channel} has been updated'

        return jsonify(default_response), 200
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(default_response), 400


@channels_blueprint.route('/channels/<slug>', methods=['DELETE'])
@jwt_required
def delete_channel(slug):
    default_response = {
        'status': 'fail',
        'message': 'Invalid payload.'
    }

    if not slug:
        return jsonify(default_response)

    try:
        channel = Channel.query.filter_by(slug=slug).first()
        if not channel:
            default_response['message'] = f'Channel {slug} not found.'
            return jsonify(default_response), 404
        else:
            db.session.delete(channel)
            db.session.commit()

            default_response['status'] = 'success'
            default_response['message'] = 'Channel has been deleted.'

            return jsonify(default_response), 200
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(default_response), 400
