
from app import ma


class ChannelSchema(ma.Schema):
    class Meta:
        fields = ('id', 'slug', 'name', 'desc', 'picture',
                  'created_at', 'updated_at', 'deleted_at')


channel_schema = ChannelSchema()
channels_schema = ChannelSchema(many=True)
