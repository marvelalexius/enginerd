import uuid
from app import db
from sqlalchemy.dialects.postgresql import UUID
from slugify import slugify
from datetime import datetime


class Channel(db.Model):
    __tablename__ = "channels"

    id = db.Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    slug = db.Column(db.String(64), nullable=False)
    name = db.Column(db.String(128), nullable=False)
    picture = db.Column(db.String(128), nullable=False)
    desc = db.Column(db.String(128), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True,
                           onupdate=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def __init__(self, name, desc, picture):
        self.name = name
        self.slug = slugify(self.name)
        self.desc = desc
        self.picture = picture
