import uuid
from datetime import datetime
from slugify import slugify
from sqlalchemy.dialects.postgresql import UUID

from app import db


class Post(db.Model):
    __tablename__ = 'posts'

    id = db.Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    channel_id = db.Column(UUID(as_uuid=True), db.ForeignKey('channels.id'),
                           nullable=False)
    title = db.Column(db.String(240), nullable=False)
    slug = db.Column(db.String(240), nullable=False)
    body = db.Column(db.String(1024), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True,
                           onupdate=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def __init__(self, channel_id, title, body):
        self.channel_id = channel_id
        self.title = title
        self.slug = slugify(self.title)
        self.body = body
