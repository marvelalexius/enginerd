
from app import ma


class PostSchema(ma.Schema):
    class Meta:
        fields = ('id', 'channel_id', 'title', 'slug', 'body',
                  'created_at', 'updated_at', 'deleted_at')


post_schema = PostSchema()
posts_schema = PostSchema(many=True)
