from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError
from app import db

from app.api.channels.model import Channel
from app.api.posts.model import Post
from app.api.comments.model import Comment
from app.api.channels.schema import channel_schema
from app.api.posts.schema import post_schema, posts_schema
from app.api.comments.schema import comments_schema

posts_blueprint = Blueprint('posts', __name__)


@posts_blueprint.route('/posts/ping', methods=['GET'])
def ping_pong():
    return jsonify({
        'status': 'success',
        'message': 'pong!'
    })


@posts_blueprint.route('/posts', methods=['GET'])
def all_posts():
    default_response = {
        'status': 'success',
        'data': {
            'channel': {},
            'posts': []
        }
    }

    page = request.args.get('page', 1, type=int)
    channels = request.args.get('channels', '')
    if channels:
        channel_list = channels.split(',')
        channel_query = Channel.query.filter(
            Channel.slug.in_(channel_list)).all()
        channel_result = channel_schema.dump(
            channel_query).data

        def get_id(x): return x.id
        channel_id_list = map(get_id, channel_result)

        posts_query = Post.query.filter(
            Post.channel_id.in_(channel_id_list)).paginate(page, 10, True).items
        posts_result = posts_schema.dump(posts_query).data

        default_response['data']['channel'] = channel_result
        default_response['data']['posts'] = posts_result

        return jsonify(default_response), 200

    posts_query = Post.query.paginate(page, 10, True).items
    default_response['data']['posts'] = posts_schema.dump(posts_query).data

    return jsonify(default_response), 200


@posts_blueprint.route('/posts', methods=['POST'])
def add_post():
    post_data = request.get_json()
    response_object = {
        'status': 'fail',
        'message': 'Invalid payload.',
        'data': None
    }
    title = post_data.get('title')
    body = post_data.get('body')
    channel_id = post_data.get('channel_id')
    if not title or not body or not channel_id:
        return jsonify(response_object), 400
    try:
        post = Post(title=title, body=body, channel_id=channel_id)
        db.session.add(post)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': f'{title} was added!',
            'data': post_schema.dump(post).data
        }
        return jsonify(response_object), 200
    except IntegrityError:
        response_object['message'] = 'Channel does not exist'
        return jsonify(response_object), 400


@posts_blueprint.route('/posts/<slug>', methods=['GET'])
def show_post(slug):
    """Get single post detail"""
    response_object = {
        'status': 'fail',
        'message': 'Post does not exist'
    }
    try:
        post = Post.query.filter_by(slug=slug).first()
        comments = Comment.query.filter_by(slug=slug).all()
        if not post:
            return jsonify(response_object), 404
        else:
            response_object = {
                'status': 'success',
                'data': {
                    'id': post.id,
                    'title': post.title,
                    'body': post.body,
                    'comments': comments_schema.dump(comments).data
                }
            }
            return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404
