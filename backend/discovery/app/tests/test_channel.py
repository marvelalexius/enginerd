import json
import unittest

from app.tests.base import BaseTestCase
from app import db
from app.api.channels.model import Channel

def add_channel(name, desc):
    channel = Channel(name=name, desc=desc)
    db.session.add(channel)
    db.session.commit()
    return channel


class TestChannelService(BaseTestCase):
    """Tests for the Channels Services."""

    def test_channels(self):
        """Ensure the /ping route behaves correctly."""
        response = self.client.get('/channels/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong!', data['message'])
        self.assertIn('success', data['status'])

    def test_add_channel(self):
        """Ensure a new channel can be added to the database."""
        with self.client:
            response = self.client.post(
                '/channels',
                data=json.dumps({
                    'name': 'JavaScript',
                    'desc': 'JavaScript developers move fast and push software development forward.'
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn('JavaScript was added!', data['message'])
            self.assertIn('success', data['status'])

    def test_add_channel_invalid_json(self):
        """Ensure error is thrown if the JSON object is empty."""
        with self.client:
            response = self.client.post(
                '/channels',
                data=json.dumps({}),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_channel_invalid_json_keys(self):
        """Ensure error is thrown if the JSON object does not have the right keys."""
        with self.client:
            response = self.client.post(
                '/channels',
                data=json.dumps({
                    'desc': 'JavaScript developers move fast and push software development forward.'
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_channel_empty_name(self):
        """Ensure error is thrown if the JSON object name is empty."""
        with self.client:
            response = self.client.post(
                '/channels',
                data=json.dumps({
                    'name': '',
                    'desc': 'JavaScript developers move fast and push software development forward.'
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_channel_duplicate_name(self):
        """Ensure error is thrown if the name already exists."""
        with self.client:
            self.client.post(
                '/channels',
                data=json.dumps({
                    'name': 'JavaScript',
                    'desc': 'JavaScript developers move fast and push software development forward.'
                }),
                content_type='application/json'
            )
            response = self.client.post(
                '/channels',
                data=json.dumps({
                    'name': 'JavaScript',
                    'desc': 'JavaScript developers move fast and push software development forward.'
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Sorry. That name already exists.', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_channel(self):
        """Ensure get single channel behaves correctly."""
        channel = add_channel('JavaScript', 'JavaScript developers move fast and push software development forward.')
        with self.client:
            response = self.client.get(f'/channels/{channel.id}')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('JavaScript', data['data']['name'])
            self.assertIn('JavaScript developers move fast and push software development forward.', data['data']['desc'])
            self.assertIn('success', data['status'])

    def test_single_channel_no_id(self):
        """Ensure error is thrown if an id is not provided."""
        with self.client:
            response = self.client.get('/channels/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Channel does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_channel_incorrect_id(self):
        """Ensure error is thrown if a channel with given id not found."""
        with self.client:
            response = self.client.get('/channels/999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Channel does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_all_channels(self):
        """Ensure get all channels behave correctly."""
        mock_name_1 = 'JavaScript'
        mock_desc_1 = 'JavaScript developers move fast and push software development forward.'
        mock_name_2 = 'ReactJS'
        mock_desc_2 = 'ReactJS is the leading framework for building web apps with JavaScript.'
        add_channel(mock_name_1, mock_desc_1)
        add_channel(mock_name_2, mock_desc_2)
        with self.client:
            response = self.client.get('/channels')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data['data']['channels']), 2)
            self.assertIn(mock_name_1, data['data']['channels'][0]['name'])
            self.assertIn(mock_desc_1, data['data']['channels'][0]['desc'])
            self.assertIn(mock_name_2, data['data']['channels'][1]['name'])
            self.assertIn(mock_desc_2, data['data']['channels'][1]['desc'])
            self.assertIn('success', data['status'])


if __name__ == '__main__':
    unittest.main()
