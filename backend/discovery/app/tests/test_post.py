import json
import unittest

from app.tests.base import BaseTestCase
from app import db
from app.api.posts.model import Post

mockPost = {
    'title': 'JavaScript is Awesome',
    'body': """It's so awesome that you will wonder everytime
             why your code works."""
}


def add_post(title, body):
    post = Post(title=title, body=body)
    db.session.add(post)
    db.session.commit()
    return post


class TestPostService(BaseTestCase):
    """Tests for the Posts services."""

    def test_posts(self):
        """Ensure the /ping route behaves correctly."""
        response = self.client.get('/posts/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong!', data['message'])
        self.assertIn('success', data['status'])

    def test_add_post(self):
        """Ensure a new post can be added to the database."""
        with self.client:
            response = self.client.post(
                '/posts',
                data=json.dumps({
                    'title': mockPost['title'],
                    'body': mockPost['body']
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            mockTitle = mockPost['title']
            self.assertEqual(response.status_code, 200)
            self.assertIn(mockPost['title'], data['data']['title'])
            self.assertIn(mockPost['body'], data['data']['body'])
            self.assertTrue(data['data']['id'])
            self.assertIn(f'{mockTitle} was added!', data['message'])
            self.assertIn('success', data['status'])

    def test_add_post_invalid_json(self):
        """Ensure error is thrown when invalid json provided"""
        with self.client:
            response = self.client.post(
                '/posts',
                data=json.dumps({}),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_post_no_title(self):
        """Ensure error is thrown when no title provided"""
        with self.client:
            response = self.client.post(
                '/posts',
                data=json.dumps({
                    'title': '',
                    'body': 'Test Body'
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_post_no_body(self):
        """Ensure error is thrown when no body provided"""
        with self.client:
            response = self.client.post(
                '/posts',
                data=json.dumps({
                    'title': 'Test Title',
                    'body': ''
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_post(self):
        """Ensure get single post behave correctly"""
        post = add_post(mockPost['title'], mockPost['body'])
        with self.client:
            response = self.client.get(f'/posts/{post.id}')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn(mockPost['title'], data['data']['title'])
            self.assertIn(mockPost['body'], data['data']['body'])
            self.assertIn('success', data['status'])

    def test_single_post_no_id(self):
        """Ensure error is thrown when id is not provided"""
        with self.client:
            response = self.client.get('/posts/blah')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Post does not exist', data['message'])
            self.assertIn('fail', data['status'])

    def test_single_post_incorrect_id(self):
        """Ensure error is thrown when id is not provided"""
        with self.client:
            response = self.client.get('/posts/9999')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Post does not exist', data['message'])
            self.assertIn('fail', data['status'])
