import json
import unittest

from app.tests.base import BaseTestCase


mockPost = {
    'title': 'JavaScript is Awesome',
    'body': "It's so awesome that you will wonder everytime why your code works."
}
mockComment = {
    'body': 'Wow, this article is aweseome!'
}


class TestCommentService(BaseTestCase):
    """Tests for the comments service."""

    def test_comments(self):
        """Ensure the /ping route for comments behaves correctly."""
        response = self.client.get('/comments/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong!', data['message'])
        self.assertIn('success', data['status'])

    def test_add_comment(self):
        """Ensure a new comment can be added to the database."""
        with self.client:
            postResponse = self.client.post(
                '/posts',
                data=json.dumps({
                    'title': mockPost['title'],
                    'body': mockPost['body']
                }),
                content_type='application/json'
            )
            postData = json.loads(postResponse.data.decode())
            postId = postData['data']['id']
            response = self.client.post(
                '/comments',
                data=json.dumps({
                    'post_id': postId,
                    'body': mockComment['body'],
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn('success', data['status'])

    def test_add_post_invalid_json(self):
        """Ensure error is thrown when invalid json provided"""
        with self.client:
            response = self.client.post(
                '/comments',
                data=json.dumps({}),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Invalid payload.', data['message'])
            self.assertIn('fail', data['status'])

    def test_add_comment_incorrect_post_id(self):
        """Ensure error is thrown when post id is not found"""
        with self.client:
            response = self.client.post(
                '/comments',
                data=json.dumps({
                    'post_id': 999,
                    'body': mockComment['body']
                }),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn('Post does not exist', data['message'])
            self.assertIn('fail', data['status'])
            self.assertFalse(data['data'])
