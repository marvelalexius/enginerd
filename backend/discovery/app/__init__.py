import os

from flask import Flask, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_redis import FlaskRedis
from flask_jwt_extended import JWTManager
from flask_elasticsearch import FlaskElasticsearch

# instantiate the extension
db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()
cache = FlaskRedis()
jwt = JWTManager()
cors = CORS()
es = FlaskElasticsearch()


def create_app(script_info=None):
    app = Flask(__name__)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # set up extensions
    cors.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
    cache.init_app(app)
    jwt.init_app(app)
    es.init_app(app)

    # register blueprint
    from app.api.channels.view import channels_blueprint
    app.register_blueprint(channels_blueprint)
    from app.api.comments.view import comments_blueprint
    app.register_blueprint(comments_blueprint)
    from app.api.posts.view import posts_blueprint
    app.register_blueprint(posts_blueprint)

    # shell context for flask cli
    app.shell_context_processor({'app': app, 'db': db})
    return app
