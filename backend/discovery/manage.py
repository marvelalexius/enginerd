import unittest

from flask.cli import FlaskGroup

from app import create_app, db
from app.api.channels.model import Channel

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command()
def recreate_db():
    """Recreate the database."""
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def seed_db():
    """Seeds the database."""
    db.session.add(Channel(name='JavaScript', desc="JavaScript developers move fast and push software development forward.",
                           picture="https://connpass-tokyo.s3.amazonaws.com/event/56727/e238f30a75ea4ed29d33e1d0aeaf5644.png"))
    db.session.add(Channel(name='ReactJS', desc="ReactJS is the leading framework for building web apps with JavaScript.",
                           picture="https://cdn.auth0.com/blog/reactjs16/logo.png"))
    db.session.commit()


@cli.command()
def test():
    """ Runs the tests without code coverage. """
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    cli()
