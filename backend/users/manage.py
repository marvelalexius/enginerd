import unittest
# import coverage

from flask.cli import FlaskGroup

from app import create_app, db
from app.api.users.model import User

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command()
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def test():
    """ Runs the tests without code coverage"""
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


@cli.command()
def seed_db():
    """Seeds the database."""
    db.session.add(User(username='michael', email="hermanmu@gmail.com",
                        password="secret1@#", roles="admin", picture=""))
    db.session.add(User(username='michaelherman', email="michael@mherman.org",
                        password="secret1@#", roles="user", picture=""))
    db.session.commit()


if __name__ == '__main__':
    cli()
