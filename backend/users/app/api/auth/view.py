from flask import Blueprint, jsonify, request
from flask_jwt_extended import (
    create_access_token, create_refresh_token, get_jwt_identity,
    jwt_required, jwt_refresh_token_required
)

from app.api.users.schema import user_schema
from app.api.users.model import User
from app import bcrypt

auth_blueprint = Blueprint('auth', __name__, template_folder='templates')


@auth_blueprint.route('/auth/login', methods=['POST'])
def authentication():
    default_response = {
        'status': 'fail',
        'message': 'Username or password is incorrect'
    }

    json_data = request.get_json()
    username = json_data.get('username')
    password = json_data.get('password')

    if not json_data or not username or not password:
        default_response['message'] = 'Form can not be empty'

        return jsonify(default_response), 400

    user = User.query.filter_by(username=username).first()

    if not user:
        default_response['message'] = 'User not found'

        return jsonify(default_response), 200

    if bcrypt.check_password_hash(user.password, password):
        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token), 200

    return jsonify(default_response), 400


@auth_blueprint.route('/auth/refresh-token', methods=['GET'])
@jwt_refresh_token_required
def refresh_token():
    token = {
        'access_token': create_access_token(identity=get_jwt_identity())
    }

    return jsonify(token), 200


@auth_blueprint.route('/auth/me', methods=['GET'])
@jwt_required
def profile():
    default_response = {
        'status': 'fail',
        'message': 'Something went wrong'
    }

    username = get_jwt_identity()

    try:
        user = User.query.filter_by(username=username).first()
        default_response = {
            'status': 'success',
            'data': user_schema.dump(user).data
        }

        return jsonify(default_response), 200
    except ValueError:
        return jsonify(default_response), 500
