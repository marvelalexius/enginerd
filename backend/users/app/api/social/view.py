from flask import jsonify
from flask_dance.consumer import oauth_authorized, oauth_error
from flask_dance.contrib.github import make_github_blueprint
from flask_dance.contrib.google import make_google_blueprint
from flask_dance.contrib.twitter import make_twitter_blueprint
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend
from flask_jwt_extended import create_access_token, create_refresh_token
from sqlalchemy.orm.exc import NoResultFound

from app import db
from app.api.users.model import OAuth, User


# Github Login
github_blueprint = make_github_blueprint(
    client_id='b09ca3dfde7b6d5632cb',
    client_secret='422d343a1105217eb225c519fe8c98aab9366498',
    backend=SQLAlchemyBackend(OAuth, db.session),
)


@oauth_authorized.connect_via(github_blueprint)
def github_logged_in(blueprint, token):
    default_response = {
        'status': 'failed',
        'message': 'Failed to login with Github'
    }

    if not token:
        return jsonify(default_response)

    resp = blueprint.session.get('/user')

    if not resp.ok:
        default_response['message'] = 'Failed to fetch user info from GitHub.'
        return jsonify(default_response)

    github_info = resp.json()
    github_user_id = str(github_info['id'])

    query = OAuth.query.filter_by(
        provider=blueprint.name,
        provider_user_id=github_user_id,
    )

    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=blueprint.name,
            provider_user_id=github_user_id,
            token=token,
        )

    if oauth.user:
        username = github_info['username']
        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)

    else:
        username = github_info['username']
        email = github_info['email']

        user = User(
            email=email,
            username=username,
            picture="",
            password="",
        )

        oauth.user = user

        db.session.add_all([user, oauth])
        db.session.commit()

        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)


@oauth_error.connect_via(github_blueprint)
def github_error(blueprint, error, error_description=None, error_uri=None):
    msg = (
        "OAuth error from {name}! "
        "error={error} description={description} uri={uri}"
    ).format(
        name=blueprint.name,
        error=error,
        description=error_description,
        uri=error_uri,
    )

    return jsonify({
        'status': 'failed',
        'message': msg
    })


# @todo: Google Login
# Google Login
google_blueprint = make_google_blueprint(
    client_id='279173192701-7bpsum9thnhrgp9tilnn1rp32ako3fgc.apps.googleusercontent.com',
    client_secret='ewnoxxU8hEXYhiy6sK47Vkfx',
    scope=["profile", "email"],
    backend=SQLAlchemyBackend(OAuth, db.session),
)


@oauth_authorized.connect_via(google_blueprint)
def google_logged_in(blueprint, token):
    default_response = {
        'status': 'failed',
        'message': 'Failed to login with Github'
    }

    if not token:
        return jsonify(default_response)

    resp = blueprint.session.get('/user')

    if not resp.ok:
        default_response['message'] = 'Failed to fetch user info from Google.'
        return jsonify(default_response)

    google_info = resp.json()
    google_user_id = str(google_info['id'])

    query = OAuth.query.filter_by(
        provider=blueprint.name,
        provider_user_id=google_user_id,
    )

    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=blueprint.name,
            provider_user_id=google_user_id,
            token=token,
        )

    if oauth.user:
        username = google_info['username']
        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)

    else:
        username = google_info['username']
        email = google_info['email']

        user = User(
            email=email,
            username=username,
            picture="",
            password="",
        )

        oauth.user = user

        db.session.add_all([user, oauth])
        db.session.commit()

        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)


@oauth_error.connect_via(github_blueprint)
def google_error(blueprint, error, error_description=None, error_uri=None):
    msg = (
        "OAuth error from {name}! "
        "error={error} description={description} uri={uri}"
    ).format(
        name=blueprint.name,
        error=error,
        description=error_description,
        uri=error_uri,
    )

    return jsonify({
        'status': 'failed',
        'message': msg
    })


# @todo: Twitter Login
# Twitter Login
twitter_blueprint = make_twitter_blueprint(
    api_key='vM3bV1S1HUglIpdmSat74cgkD',
    api_secret='zvu6VtT8jjPyzMletPK2ajFEnUQjWSLSkIdVvvg4nMA6Xpafwu',
    backend=SQLAlchemyBackend(OAuth, db.session),
)


@oauth_authorized.connect_via(twitter_blueprint)
def twitter_logged_in(blueprint, token):
    default_response = {
        'status': 'failed',
        'message': 'Failed to login with Twitter'
    }

    if not token:
        return jsonify(default_response)

    resp = blueprint.session.get('/user')

    if not resp.ok:
        default_response['message'] = 'Failed to fetch user info from GitHub.'
        return jsonify(default_response)

    twitter_info = resp.json()
    twitter_user_id = str(twitter_info['id'])

    query = OAuth.query.filter_by(
        provider=blueprint.name,
        provider_user_id=twitter_user_id,
    )

    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=blueprint.name,
            provider_user_id=twitter_user_id,
            token=token,
        )

    if oauth.user:
        username = twitter_info['username']
        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)

    else:
        username = twitter_info['username']
        email = twitter_info['email']

        user = User(
            email=email,
            username=username,
            picture="",
            password="",
        )

        oauth.user = user

        db.session.add_all([user, oauth])
        db.session.commit()

        token = {
            'access_token': create_access_token(identity=username),
            'refresh_token': create_refresh_token(identity=username)
        }

        return jsonify(token)


@oauth_error.connect_via(twitter_blueprint)
def twitter_error(blueprint, error, error_description=None, error_uri=None):
    msg = (
        "OAuth error from {name}! "
        "error={error} description={description} uri={uri}"
    ).format(
        name=blueprint.name,
        error=error,
        description=error_description,
        uri=error_uri,
    )

    return jsonify({
        'status': 'failed',
        'message': msg
    })
