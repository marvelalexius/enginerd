from functools import wraps
from flask import jsonify
from flask_jwt_extended import (
    verify_jwt_in_request,
    get_jwt_identity
)

from app.api.users.model import User


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        username = get_jwt_identity()
        default_response = {
            'status': 'failed',
            'message': 'You don\'t have enough permission.'
        }

        try:
            user = User.query.filter_by(username=username).first()
            if user.roles != 'admin':
                return jsonify(default_response), 403
            else:
                return fn(*args, **kwargs)
        except ValueError:
            default_response['message'] = 'Fatal error.'
            return jsonify(default_response), 500

    return wrapper
