import json
from sqlalchemy import exc

from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required

from app.api.users.model import User
from app.api.users.schema import users_schema, user_schema
from app.api.users.middleware import admin_required
from app import db, cache


users_blueprint = Blueprint('users', __name__, template_folder='templates')


@users_blueprint.route('/users', methods=['GET'])
@jwt_required
@admin_required
def all_users():
    default_response = {
        'status': 'success',
        'data': {
            'users': []
        }
    }

    users = request.args.get('users', '')
    if users:
        user_list = users.split(',')
        users_query = User.query.filter(User.username.in_(user_list)).all()
        default_response['data']['users'] = users_schema.dump(users_query).data
        return jsonify(default_response), 200

    page = request.args.get('page', 1, type=int)
    users_query = User.query.paginate(
        page, 10, True
    ).items
    default_response['data']['users'] = users_schema.dump(users_query).data

    return jsonify(default_response), 200


@users_blueprint.route('/users/<username>', methods=['GET'])
@jwt_required
@admin_required
def show_user(username):
    """Get single user details"""
    default_response = {
        'status': 'fail',
        'message': 'User does not exist'
    }

    cache_key = 'show_user_' + username
    cache_result = cache.get(cache_key)
    if cache_result:
        default_response = json.loads(cache_result)
        return jsonify(default_response), 200

    try:
        user = User.query.filter_by(username=username).first()
        if not user:
            return jsonify(default_response), 404
        else:
            default_response = {
                'status': 'success',
                'data': user_schema.dump(user).data
            }
            cache.set(cache_key, json.dumps(default_response))

            return jsonify(default_response), 200
    except ValueError:
        return jsonify(default_response), 404


@users_blueprint.route('/users', methods=['POST'])
@jwt_required
@admin_required
def add_user():
    json_data = request.get_json()
    default_response = {
        'status': 'fail',
        'message': 'Invalid payload.'
    }

    username = json_data.get('username')
    email = json_data.get('email')
    password = json_data.get('password')
    picture = json_data.get('picture')

    if not username or not email or not password:
        return jsonify(default_response), 400

    try:
        user = User.query.filter_by(email=email).filter_by(
            username=username).first()
        if not user:
            db.session.add(User(username=username, email=email,
                                password=password, picture=picture))
            db.session.commit()

            default_response['status'] = 'success'
            default_response['message'] = f'{email} was added!'
            return jsonify(default_response), 201
        else:
            default_response['message'] = 'Sorry. That email already exists.'
            return jsonify(default_response), 400
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(default_response), 400


@users_blueprint.route('/users/<username>', methods=['PUT'])
@jwt_required
@admin_required
def edit_user(username):
    json_data = request.get_json()
    default_response = {
        'status': 'fail',
        'message': 'Invalid payload.'
    }

    if not json_data:
        return jsonify(default_response), 400

    email = json_data.get('email')
    password = json_data.get('password')
    picture = json_data.get('picture')

    if not username or not email or not password:
        return jsonify(default_response), 400

    try:
        user = User.query.filter_by(username=username).first()
        if not user:
            default_response['message'] = f'Username {username} not found.'
            return jsonify(default_response), 404
        else:
            user.username = username
            user.email = email
            user.password = password
            user.picture = picture

            db.session.commit()

            cache_key = 'show_user_' + username
            cache.delete(cache_key)

        default_response['status'] = 'success'
        default_response['message'] = f'{username} has been updated'

        return jsonify(default_response), 200
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(default_response), 400


@users_blueprint.route('/users/<username>', methods=['DELETE'])
@jwt_required
@admin_required
def delete_user(username):
    default_response = {
        'status': 'fail',
        'message': 'Invalid payload.'
    }

    if not username:
        return jsonify(default_response)

    try:
        user = User.query.filter_by(username=username).first()
        if not user:
            default_response['message'] = f'Username {username} not found.'
            return jsonify(default_response), 404
        else:
            db.session.delete(user)
            db.session.commit()

            default_response['status'] = 'success'
            default_response['message'] = 'User has been deleted.'

            return jsonify(default_response), 200
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify(default_response), 400
