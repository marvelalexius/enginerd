import uuid
from datetime import datetime
from sqlalchemy.dialects.postgresql import UUID
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin

from app import db, bcrypt


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    username = db.Column(db.String(15), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    roles = db.Column(db.String(10), nullable=False, default='user')
    picture = db.Column(db.String(150), nullable=True)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True,
                           onupdate=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def __init__(self, username, email, password, roles, picture):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password).decode()
        self.roles = roles
        self.picture = picture


class OAuth(OAuthConsumerMixin, db.Model):
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey(User.id))
    user = db.relationship(User)
