import os

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_redis import FlaskRedis


# instantiate the extension
db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()
bcrypt = Bcrypt()
jwt = JWTManager()
cache = FlaskRedis()
cors = CORS()


def create_app(script_info=None):
    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # set up extensions
    cors.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)
    cache.init_app(app)

    # register blueprints
    from app.api.users.view import users_blueprint
    app.register_blueprint(users_blueprint)

    from app.api.auth.view import auth_blueprint
    app.register_blueprint(auth_blueprint)

    from app.api.social.view import (
        github_blueprint,
        google_blueprint,
        twitter_blueprint,
    )

    app.register_blueprint(github_blueprint, url_prefix="/authorize")
    app.register_blueprint(google_blueprint, url_prefix="/authorize")
    app.register_blueprint(twitter_blueprint, url_prefix="/authorize")

    # shell context for flask cli
    app.shell_context_processor({'app': app, 'db': db})
    return app
