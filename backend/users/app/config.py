import os


class BaseConfig:
    """Base configuration"""
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'my_precious'
    JWT_SECRET_KEY = 'supersecret'
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    # SOCIAL AUTH CONFIG
    GITHUB_CLIENT_ID = 'b09ca3dfde7b6d5632cb'
    GITHUB_CLIENT_SECRET = '422d343a1105217eb225c519fe8c98aab9366498'
    GOOGLE_CLIENT_ID = '279173192701-7bpsum9thnhrgp9tilnn1rp32ako3fgc.apps.googleusercontent.com'
    GOOGLE_CLIENT_SECRET = 'ewnoxxU8hEXYhiy6sK47Vkfx'
    TWITTER_API_KEY = 'vM3bV1S1HUglIpdmSat74cgkD'
    TWITTER_API_SECRET = 'zvu6VtT8jjPyzMletPK2ajFEnUQjWSLSkIdVvvg4nMA6Xpafwu'


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    OAUTHLIB_INSECURE_TRANSPORT = 1
    REDIS_URL = os.environ.get('REDIS_URL')


class TestingConfig(BaseConfig):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL')


class ProductionConfig(BaseConfig):
    """Production configuration"""
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    REDIS_URL = os.environ.get('REDIS_URL')
