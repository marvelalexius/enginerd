# Backend Workflow

Before developing backend, you required to install [https://docs.pipenv.org/](pipenv)
Workflow rules:

- First make sure you have activated `virtualenv`
- After you installed pipenv, cd to backend folder service, eg: `cd backend/discovery` then run `pipenv shell` to activate the `virtualenv`
- Adding package use `pipenv install <package-name>`, other commands can be seen on [https://docs.pipenv.org/](pipenv) docs.
- Once the package has been added into `Pipfile`, rebuild the docker image by running `enginerd rebuild` from root directory.
- If you want to open Merge Request, please make sure the backend build NOT BROKEN. (You can merge as long as the build is not broken)

That's all.
