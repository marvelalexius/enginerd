import get from 'lodash/get';
import configList from './configList';

const isServer = typeof window === 'undefined';

export default function getConfig(key) {
  const source = isServer ? configList : window.Enginerd;
  return get(source, key, '');
}
