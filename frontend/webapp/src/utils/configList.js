export default {
  hostname: {
    account: process.env.ACCOUNT_HOSTNAME || 'http://192.168.99.100:5001',
    discovery: process.env.DISCOVERY_HOSTNAME || 'http://192.168.99.100:5002',
    web: process.env.WEBAPP_HOSTNAME
  }
};
