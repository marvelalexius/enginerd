const COUNT_ABBRS = ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];

function abbreviateNumber(count, decimals = 2) {
  const i = 0 === count ? count : Math.floor(Math.log(count) / Math.log(1000));
  let result = parseFloat((count / Math.pow(1000, i)).toFixed(decimals));
  result += `${COUNT_ABBRS[i]}`;

  return result;
}

export default abbreviateNumber;
