import axios from 'axios';

export default class EnginerdAPI {
  constructor(baseURL, token) {
    this.axios = axios.create({
      baseURL: baseURL,
      headers: {
        Authorization: `Bearer ${token || ''}`,
        'X-User-Agent': `Enginerd Web App 1.0`
      }
    });
  }

  get(pathname, config) {
    return this.axios.get(pathname, config);
  }

  post(pathname, data, config) {
    return this.axios.post(pathname, data, config);
  }

  put(pathname, data, config) {
    return this.axios.put(pathname, data, config);
  }

  delete(pathname, config) {
    return this.axios.delete(pathname, config);
  }
}
