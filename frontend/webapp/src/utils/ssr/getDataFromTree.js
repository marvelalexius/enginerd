import treeWalker from 'react-tree-walker';

export default function fetchComponentData(components, params, store) {
  const values = [];

  const visitor = (_, instance) => {
    if (instance && typeof instance.getInitialData !== 'undefined') {
      values.push(instance.getInitialData);
    }
  };

  return treeWalker(components, visitor).then(() => {
    const promises = values.map(getInitialData =>
      store.dispatch(getInitialData(params, store.getState()))
    );

    return Promise.all(promises);
  });
}
