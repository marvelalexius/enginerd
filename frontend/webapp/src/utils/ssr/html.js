const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);
import { flexbox } from './styles';
import configList from '../configList';

export default function html(markup, state) {
  return `<!doctype html>
    <html lang="">
    <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style id="flexboxgrid">${flexbox}</style>
      ${
        assets.client.css
          ? `<link rel="stylesheet" href="${assets.client.css}">`
          : ''
      }
    </head>
    <body>
      <div id="root">${markup}</div>
      <script id="preloaded-state">
          window.State = ${JSON.stringify(state).replace(/</g, '\\u003c')}
          window.Enginerd = ${JSON.stringify(configList).replace(
            /</g,
            '\\u003c'
          )}
        </script>
      ${
        process.env.NODE_ENV === 'production'
          ? `<script src="${assets.client.js}" defer></script>`
          : `<script src="${assets.client.js}" defer crossorigin></script>`
      }
    </body>
  </html>`;
}
