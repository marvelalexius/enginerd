import express from 'express';
import cookieParser from 'cookie-parser';
import cookiesMiddleware from 'universal-cookie-express';

import universalRender from './middlewares/universalRender';

const app = express();

app.disable('x-powered-by');

app.use(cookieParser());
app.use(express.static(process.env.RAZZLE_PUBLIC_DIR));
app.use(cookiesMiddleware());
app.use(universalRender);

export default app;
