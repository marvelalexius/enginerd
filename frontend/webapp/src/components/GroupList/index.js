import React from 'react';
import { NavLink, Link } from 'react-router-dom';

import s from './style.module.css';

export default function GroupList(props) {
  return (
    <div>
      <ul className={s.list}>
        {props.menu.map((m, i) => (
          <li key={i}>
            <NavLink to={`/t/${m.slug}`}>
              <img className={s.tagLogo} src={m.picture} />
              <div className={s.tagText}>{m.title}</div>
              {m.newUpdates && <span className={s.newUpdatesBadge} />}
            </NavLink>
          </li>
        ))}
      </ul>
      <Link className={s.moreTag} to="/">
        More
      </Link>
    </div>
  );
}
