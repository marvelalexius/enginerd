import React from 'react';

const classMap = {
  xs: 'col-xs',
  sm: 'col-sm',
  md: 'col-md',
  lg: 'col-lg',
  xsOffset: 'col-xs-offset',
  smOffset: 'col-sm-offset',
  mdOffset: 'col-md-offset',
  lgOffset: 'col-lg-offset'
};

function getClassNames(props) {
  const extraClassNames = [];

  if (props.className) {
    extraClassNames.push(props.className);
  }

  return Object.keys(props)
    .filter(key => classMap[key])
    .map(key => `${classMap[key]}-${props[key]}`)
    .concat(extraClassNames)
    .join(' ');
}

function Col(props) {
  const cssClass = getClassNames(props);
  return <div className={cssClass}>{props.children}</div>;
}

export default Col;
