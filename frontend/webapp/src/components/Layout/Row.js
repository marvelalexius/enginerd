import React from 'react';
import cn from 'classnames';

function Row(props) {
  const extraClass = props.className && props.className;
  const reverseClass = props.reverse && 'reverse';

  return (
    <div className={cn('row', reverseClass, extraClass)}>{props.children}</div>
  );
}

export default Row;
