import React from 'react';
import { string } from 'prop-types';

import s from './style.module.css';

export default function ItemExplore(props) {
  return (
    <div className={s.item}>
      <div
        className={s.cover}
        style={{
          backgroundImage: `url(${props.cover})`,
          backgroundColor: props.coverBGColor
        }}
      />
      <div className={s.itemDesc}>
        <figure>
          <img src={props.picture} alt={`enginerd.co ${props.title}`} />
        </figure>
        <h3>{props.title}</h3>
        <p>{props.description}</p>
      </div>
      <div className={s.itemFollow}>
        <button className={s.buttonFollow}>Follow</button>
      </div>
    </div>
  );
}

ItemExplore.defaultProps = {
  coverBGColor: '',
  cover: '',
  picture: ''
};

ItemExplore.propTypes = {
  coverBGColor: string,
  cover: string,
  picture: string,
  title: string.isRequired,
  description: string.isRequired
};
