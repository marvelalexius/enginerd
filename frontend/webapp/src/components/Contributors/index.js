import React from 'react';
import { arrayOf, object } from 'prop-types';

import s from './style.module.css';

export default function AnsweredBy(props) {
  const people = props.data.map((person, i) => (
    <li key={i}>
      <img src={person.picture} alt={person.name} />
    </li>
  ));

  return (
    <div className={s.answeredBy}>
      <p>Answered by</p>
      <ul>{people}</ul>
    </div>
  );
}

AnsweredBy.propTypes = {
  data: arrayOf(object).isRequired
};
