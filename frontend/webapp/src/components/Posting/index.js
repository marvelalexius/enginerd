import React from 'react';

import s from './style.module.css';

export default function Posting(props) {
  return (
    <div className={s.posting}>
      <div className={s.userInfo}>
        <img
          className={s.userPictOnPosting}
          src="https://randomuser.me/api/portraits/men/68.jpg"
        />
        <p className={s.userNameOnPosting}>Putu Alfred Crosby</p>
      </div>
      <div className={s.postingTextField}>What is your question?</div>
    </div>
  );
}
