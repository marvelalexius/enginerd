import React from 'react';
import { Link } from 'react-router-dom';

import { Grid } from '../layout';

import s from './style.module.css';

export default function Footer() {
  return (
    <footer className={s.footerContainer}>
      <section className={s.footer}>
        <Grid>
          <nav className={s.footerNav}>
            <ul>
              <li>
                <Link to="/">About Us</Link>
              </li>
              <li>
                <Link to="/">Help Center</Link>
              </li>
              <li>
                <Link to="/">Privacy Policy</Link>
              </li>
              <li>
                <Link to="/">Terms of Service</Link>
              </li>
              <li>
                <Link to="/">Patreon</Link>
              </li>
              <li>
                <Link to="/">Feature Request</Link>
              </li>
              <li>
                <Link to="/">Report Bugs</Link>
              </li>
              <li>
                <Link to="/">Supported Community</Link>
              </li>
            </ul>
          </nav>
        </Grid>
      </section>
      <section className={s.copyright}>
        <Grid>
          <div className={s.copyrightMain}>
            <h1 className={s.logo}>
              <Link to="/">
                <i className="fa fa-laptop" /> enginerd.co
              </Link>
            </h1>
            <div className={s.copyrightContent}>
              <p>&copy; 2018 enginerd.co</p>
            </div>
          </div>
        </Grid>
      </section>
    </footer>
  );
}
