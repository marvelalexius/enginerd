import React from 'react';
import { Link } from 'react-router-dom';
import { arrayOf, object } from 'prop-types';

import AnsweredBy from '../contributors';
import Tags from '../tags';

import s from './style.module.css';

export default function Post(props) {
  const feeds = props.data.map((f, i) => {
    return (
      <Link key={i} className={s.post} to={`/p/${f.slug}`}>
        <div className={s.postMeta}>
          <AnsweredBy data={f.answeredBy.persons} />
          <Tags data={f.tags} />
        </div>
        <h3 className={s.postTitle}>{f.title}</h3>
        <div className={s.answeredBy}>
          <div className={s.answeredByPicture}>
            <img
              src="https://randomuser.me/api/portraits/men/89.jpg"
              alt="user"
            />
          </div>
          <div className={s.answeredByBio}>
            <p>
              <Link to="/">{f.author.name}</Link>
            </p>
            <p>{f.author.bio}</p>
          </div>
        </div>
        <main className={s.content}>{f.content}</main>
        <footer className={s.buttons}>
          <div className={s.actionButton}>
            <button>
              <i className="fa fa-thumbs-o-up" aria-hidden="true" /> Upvote
            </button>
            <button>
              <i className="fa fa-pencil" aria-hidden="true" /> Answer
            </button>
            <button>
              <i className="fa fa-bookmark-o" aria-hidden="true" /> Bookmark
            </button>
          </div>
          <div className={s.shareButton}>
            <button>
              <i className="fa fa-ellipsis-h" aria-hidden="true" />
            </button>
          </div>
        </footer>
      </Link>
    );
  });

  return <div>{feeds}</div>;
}

Post.propTypes = {
  data: arrayOf(object).isRequired
};
