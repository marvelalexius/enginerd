import React from 'react';

import abbreviateNumber from '../../utils/abbreviate-number';

import s from './style.module.css';

export default function Stats(props) {
  return (
    <aside className={s.stats}>
      <div className={s.totalPost}>
        <h3>Total Post</h3>
        <p>{abbreviateNumber(props.totalPost)}</p>
      </div>
      <div className={s.totalMember}>
        <h3>Member</h3>
        <p>{abbreviateNumber(props.totalMember)}</p>
      </div>
    </aside>
  );
}
