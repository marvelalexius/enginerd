import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import { compose } from 'redux';

const PublicOnlyRoute = ({
  component: Component,
  isAuthenticated,
  cookies,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        return isAuthenticated && cookies.get('access_token') ? (
          <Redirect to="/" />
        ) : (
          <Component {...props} />
        );
      }}
    />
  );
};

const mapStateToProps = ({ user }) => ({
  isAuthenticated: user.isAuthenticated
});

export default compose(
  connect(mapStateToProps),
  withCookies
)(PublicOnlyRoute);
