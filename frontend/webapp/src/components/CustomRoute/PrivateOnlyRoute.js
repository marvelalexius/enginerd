import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import { compose } from 'redux';

const PrivateRouteComponent = ({
  component: Component,
  isAuthenticated,
  cookies,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        return isAuthenticated && cookies.get('access_token') ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        );
      }}
    />
  );
};

const mapStateToProps = ({ user }) => ({
  isAuthenticated: user.isAuthenticated
});

export default compose(
  connect(mapStateToProps),
  withCookies
)(PrivateRouteComponent);
