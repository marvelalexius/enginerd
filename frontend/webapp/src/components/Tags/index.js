import React from 'react';
import { Link } from 'react-router-dom';
import { arrayOf, object } from 'prop-types';

import s from './style.module.css';

export default function Tags(props) {
  const tags = props.data.length > 3 ? props.data.slice(0, 3) : props.data;
  const tagList = tags.map((t, i) => (
    <Link key={i} to={`/t/${t.slug}`}>
      {t.name}
    </Link>
  ));

  return (
    <div className={s.tags}>
      <p>in</p>
      {tagList}
      {props.data.length > 3 && (
        <Link to="#">+{props.data.length - tags.length}</Link>
      )}
      <button>
        <i className="fa fa-pencil" />
      </button>
    </div>
  );
}

Tags.propTypes = {
  data: arrayOf(object).isRequired
};
