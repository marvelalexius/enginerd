import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';

import { Grid, Col } from '../layout';
import s from './style.module.css';

export default function Header() {
  return (
    <Grid>
      <Col md={12}>
        <header className={cn(s.blockedDisplay, s.header)}>
          <h1 className={s.logo}>
            <Link to="/">
              <i className="fa fa-laptop" /> enginerd.co
            </Link>
          </h1>
          <nav className={s.search}>
            <label>
              <i className="fa fa-search" />
            </label>
            <input
              type="search"
              id="search-forum"
              placeholder="Try search ReactJS, Django, Rails, Hadoop..."
            />
          </nav>
          <nav className={s.nav}>
            <Link to="/" className={s.askQuestion}>
              <i className="fa fa-pencil-square-o" /> Ask Question
            </Link>
          </nav>
          <nav className={s.nav}>
            <Link to="/messages" title="Messages">
              <i className="fa fa-comments-o" />
            </Link>
            <Link to="/notifications" title="Notifications">
              <i className="fa fa-bell-o" />
            </Link>
            <Link to="/more" title="Settings">
              <i className="fa fa-ellipsis-h" />
            </Link>
          </nav>
          <nav className={s.nav}>
            <Link to="/amas" title="Explore">
              AMA
            </Link>
            <a className={s.divider}>&middot;</a>
            <Link to="/explore" title="Explore">
              Explore
            </Link>
          </nav>
        </header>
      </Col>
    </Grid>
  );
}
