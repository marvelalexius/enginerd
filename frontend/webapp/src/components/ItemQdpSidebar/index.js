import React from 'react';
import { Link } from 'react-router-dom';

import s from './style.module.css';

export default function QuestionDetailPage(props) {
  const dataList = props.data.map(datum => (
    <li>
      <Link to={`/p/${datum.slug}`}>{datum.title}</Link>
    </li>
  ));

  return (
    <aside className={s.sidebar}>
      <h3>{props.title}</h3>
      <ul>{dataList}</ul>
    </aside>
  );
}
