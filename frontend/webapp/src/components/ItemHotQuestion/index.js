import React from 'react';
import { string, arrayOf, object } from 'prop-types';
import { Link } from 'react-router-dom';

import AnsweredBy from '../contributors';
import Tags from '../tags';

import s from './style.module.css';

export default function ItemHotQuestion(props) {
  return (
    <div className={s.item}>
      <div className={s.meta}>
        <AnsweredBy data={props.answeredBy} />
        <Tags data={props.tags} />
      </div>
      <h3>{props.title}</h3>
      <p>
        <Link to="/n/:user_id">{props.user}</Link>
      </p>
    </div>
  );
}

ItemHotQuestion.propTypes = {
  title: string.isRequired,
  user: string.isRequired,
  tags: arrayOf(object).isRequired,
  answeredBy: arrayOf(object).isRequired
};
