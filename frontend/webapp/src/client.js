import React from 'react';
import { hydrate } from 'react-dom';
import { ConnectedRouter as Router } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';

import App from './App';
import configureStore from './stores';

const history = createBrowserHistory();
const initialState = window.State || {};
const store = configureStore(initialState, history);

hydrate(
  <Provider store={store}>
    <CookiesProvider>
      <Router history={history}>
        <App />
      </Router>
    </CookiesProvider>
  </Provider>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept();
}
