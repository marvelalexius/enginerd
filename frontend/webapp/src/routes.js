import React from 'react';

import Welcome from './pages/welcome';
import Home from './pages/home';
import Search from './pages/search';
import Tag from './pages/tag';
import Post from './pages/post';
import People from './pages/people';

export const routes = [
  {
    path: '/',
    component: Home,
    name: 'Home',
    exact: true
  },
  {
    path: '/auth',
    component: Welcome,
    name: 'Welcome',
    exact: true
  },
  {
    path: '/search',
    component: Search,
    name: 'Search',
    exact: false
  },
  {
    path: '/t/:slug',
    component: Tag,
    name: 'Tag',
    exact: false
  },
  {
    path: '/p/:slug',
    component: Post,
    name: 'Post',
    exact: false
  },
  {
    path: '/n/:slug',
    component: People,
    name: 'People',
    exact: false
  },
  {
    path: '/me',
    component: () => <h1>Hello</h1>,
    name: 'Profile',
    exact: false
  }
];

export const redirects = [];
