import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import user from './user';

export const actions = {
  ...user.actions
};

export const rootReducer = combineReducers({
  router,
  user: user.reducer
});
