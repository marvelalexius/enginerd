const SET_USER = '@@enginerd/auth/SET_USER';
const LOGIN = '@@enginerd/auth/LOGIN';
const LOGOUT = '@@enginerd/auth/LOGOUT';

export default {
  SET_USER,
  LOGIN,
  LOGOUT
};
