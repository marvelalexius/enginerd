import types from './types';

const reduceSetUser = (state, action) => ({
  ...state,
  data: action.payload,
  isAuthenticated: true
});

const logout = (state, action) => ({
  ...state,
  data: {},
  isAuthenticated: false
});

const initialState = {
  isAuthenticated: false,
  data: {}
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER:
      return reduceSetUser(state, action);
    case types.LOGOUT:
      return logout(state, action);
    default:
      return state;
  }
};

export default userReducer;
