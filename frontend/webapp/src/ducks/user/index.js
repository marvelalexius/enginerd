import types from './types';
import * as actions from './actions';
import reducer from './reducer';

export default {
  types,
  actions,
  reducer
};
