import Cookies from 'universal-cookie';
import EnginerdAPI from '../../utils/api';
import getConfig from '../../utils/getConfig';
import types from './types';

const hostname = getConfig('hostname.account');
const cookies = new Cookies();
const apiError = (scope, err) => {
  console.error(`[ERROR][${scope}] ${err}`);
};

export function getUser(token) {
  const API = new EnginerdAPI(hostname, token);
  return dispatch =>
    API.get('/auth/me')
      .then(({ data }) => {
        dispatch({
          type: types.SET_USER,
          payload: data.data
        });
      })
      .catch(err => apiError('getUser', err));
}

export function doLogin(payload) {
  const API = new EnginerdAPI(hostname, '');
  return _ =>
    API.post('/auth/login', payload)
      .then(({ data }) => {
        if (data.access_token) {
          cookies.set('access_token', data.access_token, { path: '/' });
          cookies.set('refresh_token', data.refresh_token, { path: '/' });

          window.location.href = getConfig('hostname.web');
        }
      })
      .catch(err => apiError('doLogin', 'Error while fetching data'));
}
