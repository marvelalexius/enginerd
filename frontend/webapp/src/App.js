import React, { Fragment, Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';

import { PrivateOnlyRoute, PublicOnlyRoute } from './components/CustomRoute';
import RedirectWithStatus from './components/Redirect';
import { actions } from './ducks';
import { routes, redirects } from './routes';

class App extends Component {
  getInitialData = (params, state) => {
    const { cookies, isAuthenticated } = this.props;
    if (!isAuthenticated) {
      return () => {};
    }

    return actions.getUser(cookies.get('access_token'));
  };

  render() {
    return (
      <Fragment>
        <Helmet
          titleTemplate="%s - Enginerd"
          defaultTitle="❤ Indonesia DEV Community"
        />
        <Switch id="app">
          {routes.map((route, index) => {
            let EnginerdRoute = Route;

            const publicOnly = ['/login'];

            const privateOnly = ['/me'];

            if (publicOnly.includes(route.path)) {
              EnginerdRoute = PublicOnlyRoute;
            }

            if (privateOnly.includes(route.path)) {
              EnginerdRoute = PrivateOnlyRoute;
            }

            return (
              <EnginerdRoute
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.component}
              />
            );
          })}

          {redirects.map(({ from, to, status }, i) => (
            <RedirectWithStatus
              key={Math.random() + 'REDIRECT_'}
              from={from}
              to={to}
              status={status}
            />
          ))}
        </Switch>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  user: user.data,
  isAuthenticated: user.isAuthenticated
});

const mapDispatchToProps = {
  getUser: actions.getUser
};

export default compose(
  withRouter,
  withCookies,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(App);
