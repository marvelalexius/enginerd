import React from 'react';
import { StaticRouter, matchPath } from 'react-router-dom';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import Cookies from 'universal-cookie';
import { createMemoryHistory } from 'history';

import App from '../App';
import configureStore from '../stores';
import html from '../utils/ssr/html';
import { routes } from '../routes';
import getDataFromTree from '../utils/ssr/getDataFromTree';

export default function universalRender(req, res) {
  const cookies = new Cookies(req.cookies);
  const history = createMemoryHistory();
  const context = {};
  const initialState = {
    user: {
      isAuthenticated: !!cookies.get('access_token')
    }
  };
  const store = configureStore(initialState, history);
  const match =
    routes.find(route => {
      return matchPath(req.originalUrl || req.url, route);
    }) || {};

  const app = (
    <Provider store={store}>
      <CookiesProvider cookies={req.universalCookies}>
        <StaticRouter location={req.url} context={context}>
          <App />
        </StaticRouter>
      </CookiesProvider>
    </Provider>
  );

  getDataFromTree(app, match.params, store)
    .then(() => {
      const markup = renderToString(app);
      if (context.url) {
        res.writeHead(301, {
          Location: context.url
        });
        res.end();
      } else {
        res.write(html(markup, store.getState()));
        res.end();
      }
    })
    .catch(error => {
      console.error('PRERENDER ERROR:', error);

      const markup = renderToString(app);
      res.write(html(markup, store.getState()));
    });
}
