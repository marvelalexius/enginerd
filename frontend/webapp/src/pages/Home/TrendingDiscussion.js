import React from 'react';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';

import s from './trendingDiscussion.module.css';
import discussions from './mock/discussion';

export default function HomeTrendingDiscussion() {
    const DiscussionList = discussions.map((discussion, i) => {
        return (
            <li key={i}>
                <NavLink to={`/discuss/${discussion.slug}`}>
                <div className={s.content}>
                    <h4>{discussion.title}</h4>
                    <div className={s.response}>
                        <i className="fa fa-commenting-o"/>  6 responses
                    </div>
                </div>
                </NavLink>
            </li>
        );
    });
    return (
        <section className={cn(s.container, s.discussion)}>
            <h3 className={s.componentTitle}>Trending Discussions</h3>
            <ul className={s.discussionList}>{DiscussionList}</ul>
        </section>
    );
}