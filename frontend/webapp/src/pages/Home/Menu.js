import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import GroupList from '../../components/GroupList';
import menu from './mock/menu';
import s from './menu.module.css';

export default function HomeMenu() {
  return (
    <div className={s.topGap}>
      <ul className={s.list}>
        <li>
          <NavLink activeClassName={s.active} to="/">
            News Feed <span className={s.newUpdatesBadge} />
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName={s.active} to="/bookmarked">
            Bookmarked
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName={s.active} to="/trends">
            Trends
          </NavLink>
        </li>
      </ul>
      <h3 className={s.shortcut}>Shortcut</h3>
      <GroupList menu={menu} />
    </div>
  );
}
