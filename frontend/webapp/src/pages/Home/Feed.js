import React from 'react';
import Posting from '../../components/Posting';
import Posts from '../../components/Posts';

import s from './feed.module.css';

export default function HomeFeed(props) {
  return (
    <section className={s.feeds}>
      <Posts data={props.posts} />
    </section>
  );
}
