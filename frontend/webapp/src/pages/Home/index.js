import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import cn from 'classnames';

import Header from '../../components/Header';
import { Grid, Row, Col } from '../../components/Layout';

import Menus from './Menu';
import Feeds from './Feed';
import TrendingDiscussions from './TrendingDiscussion';

import s from './style.module.css';

import feed from './mock/feed.json';

class Home extends React.Component {
  getInitialData = (params, state) => {
    return () => {};
  };

  render() {
    return (
      <div>
        <Header />
        <Grid className={cn(s.homePage, s.topGap)}>
          <Row>
            <Col xs={2} md={2}>
              <Menus />
            </Col>
            <Col xs={6} md={6}>
              <Feeds posts={feed} />
            </Col>
            <Col xs={4} md={4}>
              <TrendingDiscussions />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = () => ({
  user: {}
});

export default compose(connect(mapStateToProps))(Home);
