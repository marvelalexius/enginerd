import React, { Component } from 'react';

import { Grid, Row, Col } from '../../components/Layout';
import Tags from '../../components/Tags';
import AnsweredBy from '../../components/Contributors';

import s from './detail.module.css';

export default class QuestionDetail extends Component {
  render() {
    const { data } = this.props;

    return (
      <main className={s.qDetail}>
        <div className={s.meta}>
          <AnsweredBy data={data.answeredBy} />
          <Tags data={data.tags} />
        </div>
        <main>
          <h1>{data.title}</h1>
          {data.description && (
            <div className={s.description}>{data.description}</div>
          )}
        </main>
        <footer className={s.textInput}>
          <Grid>
            <Row>
              <Col sm={9} smOffset={3}>
                <div className={s.sendArea}>
                  <div className={s.send}>
                    <div
                      className={s.textArea}
                      contenteditable
                      placeholder="Answer..."
                    >
                      {' '}
                    </div>
                    <button className={s.sendBtn}>
                      <i className="fa fa-send" />
                    </button>
                  </div>
                  <div className={s.sendOptions}>
                    <input type="checkbox" value="1" id="enterToReply" />
                    <label htmlFor="enterToReply">Enter to reply</label>
                  </div>
                </div>
              </Col>
            </Row>
          </Grid>
        </footer>
      </main>
    );
  }
}
