import React from 'react';
import { Helmet } from 'react-helmet';

import Header from '../../components/Header';
import { Grid, Row, Col } from '../../components/Layout';
import QuestionSidebar from '../../components/ItemQdpSidebar';

import QuestionDetail from './Detail';

import s from './style.module.css';

import sidebarData from './mock/sidebar.json';
import question from './mock/question.json';

export default function PostDetail() {
  return (
    <div>
      <Header />
      <Grid className={s.postDetail}>
        <Row>
          <Col sm={3}>
            <div className={s.sidebarFixed}>
              <QuestionSidebar title="Similar Question" data={sidebarData} />
              <QuestionSidebar
                title="Trending in this topic"
                data={sidebarData}
              />
            </div>
          </Col>
          <Col sm={9}>
            <QuestionDetail data={question} />
          </Col>
        </Row>
      </Grid>
      <Helmet>
        <body className="question-detail-page" />
      </Helmet>
    </div>
  );
}
