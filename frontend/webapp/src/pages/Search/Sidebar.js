import React from 'react';
import { Link } from 'react-router-dom';

import s from './sidebar.module.css';

export default function Sidebar() {
  return (
    <div className={s.filterContainer}>
      <div className={s.filter}>
        <h3>By Type</h3>
        <ul>
          <li className={s.selected}>
            <Link to="/search">Question</Link>
          </li>
          <li>
            <Link to="/search">People</Link>
          </li>
        </ul>
      </div>
      <div className={s.filter}>
        <h3>By Time</h3>
        <ul>
          <li className={s.selected}>
            <Link to="/search">All Time</Link>
          </li>
          <li>
            <Link to="/search">Past Hour</Link>
          </li>
          <li>
            <Link to="/search">Past Day</Link>
          </li>
          <li>
            <Link to="/search">Past Week</Link>
          </li>
          <li>
            <Link to="/search">Past Month</Link>
          </li>
          <li>
            <Link to="/search">Past Year</Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
