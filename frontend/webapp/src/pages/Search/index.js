import React from 'react';

import Header from '../../components/Header';
import { Grid, Row, Col } from '../../components/Layout';

import Sidebar from './Sidebar';
import SearchResult from './SearchResult';

import search from './mock/search.json';

import s from './style.module.css';

export default function Home() {
  return (
    <div>
      <Header />
      <section className={s.searchTerm}>
        <Grid>
          <h2>
            Found 18 result for <strong>"Metta Handika"</strong>
          </h2>
        </Grid>
      </section>
      <Grid>
        <Row>
          <Col sm={2} md={2}>
            <Sidebar />
          </Col>
          <Col sm={6} md={6}>
            <SearchResult searchResult={search} />
          </Col>
        </Row>
      </Grid>
    </div>
  );
}
