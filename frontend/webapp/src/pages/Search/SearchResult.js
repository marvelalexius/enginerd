import React from 'react';
import Posts from '../../components/Posts';

import s from './search-result.module.css';

export default function SearchResult(props) {
  return (
    <section className={s.srp}>
      <Posts data={props.searchResult} />
    </section>
  );
}
