import React from 'react';
import { NavLink } from 'react-router-dom';

import GroupList from '../../components/GroupList';
import menu from './mock/menu';

import s from './menu.module.css';

export default function Menu() {
  return (
    <nav>
      <ul className={s.list}>
        <li>
          <NavLink to="/t/javascript" activeClassName={s.active}>
            Javascript
          </NavLink>
        </li>
        <li>
          <NavLink to="/t/javascript/member" activeClassName={s.active}>
            Member
          </NavLink>
        </li>
      </ul>
      <h3 className={s.shortcut}>Shortcut</h3>
      <GroupList menu={menu} />
    </nav>
  );
}
