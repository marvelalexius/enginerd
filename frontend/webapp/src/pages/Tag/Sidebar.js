import React from 'react';
import Stats from '../../components/Stats';
// import Widget from '../../components/widget';

import s from './style.module.css';

export default function Sidebar() {
  return (
    <div className={s.sidebar}>
      <Stats totalPost={12253} totalMember={3654} />
    </div>
  );
}
