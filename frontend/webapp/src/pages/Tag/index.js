import React from 'react';

import Header from '../../components/Header';
import { Grid, Row, Col } from '../../components/Layout';

import Menu from './Menu';
import Detail from './Detail';
import Feeds from './Feed';
import Sidebar from './Sidebar';

import s from './style.module.css';

import detail from './mock/detail.json';
import feeds from './mock/feed.json';
// import chatroom from './mock/chatroom.json';

export default function Tag() {
  return (
    <div>
      <Header />
      <Grid className={s.tagDetailPage}>
        <Row>
          <Col xs={2} md={2}>
            <Menu />
          </Col>
          <Col xs={10} md={10}>
            <Row>
              <Col xs={12} md={12}>
                <Detail {...detail} />
              </Col>
            </Row>
            <Row className={s.content}>
              <Col xs={7} md={7}>
                <Feeds posts={feeds} />
              </Col>
              <Col xs={5} md={5}>
                <Sidebar />
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </div>
  );
}
