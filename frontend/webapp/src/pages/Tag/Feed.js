import React from 'react';
import Posting from '../../components/Posting';
import Posts from '../../components/Posts';

import s from './feed.module.css';

export default function TagFeed(props) {
  return (
    <section>
      <Posting />
      <Posts data={props.posts} />
    </section>
  );
}
