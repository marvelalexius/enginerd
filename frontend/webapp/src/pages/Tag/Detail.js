import React from 'react';

import s from './detail.module.css';

export default function TagDetail(props) {
  const coverStyle = {
    backgroundImage: `url(${props.cover})`
  };

  return (
    <div className={s.container}>
      <div className={s.cover} style={coverStyle} />
      <div className={s.picture}>
        <figure>
          <img src={props.picture} />
        </figure>
        <div className={s.description}>
          <h3>{props.name}</h3>
          <p>{props.description}</p>
        </div>
      </div>
      <div className={s.additionalMenu}>
        <ul>
          <li>
            <button className={s.leave}>Leave</button>
          </li>
          <li>
            <button>Notifications</button>
          </li>
          <li>
            <button>
              <i className="fa fa-share" /> Share
            </button>
          </li>
          <li>
            <button>
              <i className="fa fa-ellipsis-h" /> More
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
}
