import React from 'react';
import cn from 'classnames';
import Input from 'react-autosize-textarea';

import s from './chatroom.module.css';

export default function ChatRoom(props) {
  const chatBubble = props.chats.map((byDate, index) => {
    const bubbles = byDate.data.map((chat, i) => (
      <div key={i} className={cn(s.bubble, chat.isMe && s.isMe)}>
        <div className={s.desc}>
          <p>{chat.name}</p>
          <p>&middot;</p>
          <p>{chat.username}</p>
          <p>&middot;</p>
          {chat.isAdmin && [<p className={s.admin}>ADMIN</p>, <p>&middot;</p>]}
          <p>{chat.timestamp}</p>
        </div>
        <div className={s.chatContent}>{chat.content}</div>
      </div>
    ));

    return (
      <div key={index} className={s.content}>
        <div className={s.date}>{byDate.date}</div>
        <div>{bubbles}</div>
      </div>
    );
  });

  return (
    <div className={s.container}>
      <div className={s.header}>
        <div className={s.search}>
          <label htmlFor="search-chatroom">
            <i className="fa fa-search" />
          </label>
          <input type="search" placeholder="Search in this chatroom..." />
        </div>
        <div className={s.info}>
          <p>Everything starts from terminal.</p>
          <div className={s.util}>
            <span>
              <i className="fa fa-user" /> 1,038
            </span>
            <button>
              <i className="fa fa-bell" />
            </button>
            <button>
              <i className="fa fa-ellipsis-h" />
            </button>
          </div>
        </div>
      </div>
      <div className={props.content}>{chatBubble}</div>
      <div className={s.textarea}>
        <button className={s.image}>
          <i className="fa fa-picture-o" />
        </button>
        <Input rows={1} maxRows={4} />
        <button className={s.send}>
          <i className="fa fa-send-o" />
        </button>
      </div>
    </div>
  );
}
