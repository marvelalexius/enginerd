import React from 'react';
import { Link } from 'react-router-dom';

import { Grid, Row, Col } from '../../components/Layout';
import ItemHotQuestion from '../../components/ItemHotQuestion';
import s from './trending.module.css';

import nerds from './mock/nerds.json';
import questions from './mock/trending.json';

export default function Trending() {
  const nerdList = nerds.map((nerd, i) => (
    <div key={i} className={s.nerd}>
      <Link to={`n/${nerd.id}`}>
        <img src={nerd.picture} alt={nerd.name} />
      </Link>
    </div>
  ));

  const trendingList = questions.map((question, i) => (
    <ItemHotQuestion key={i} {...question} />
  ));

  return (
    <section className={s.trendingContainer}>
      <Grid>
        <Row>
          <Col sm={6}>
            <div className={s.trendingDiscussion}>
              <div className={s.title}>
                <h2>Trending Discussions</h2>
                <p>Popular discussions and technologies on Nginerd</p>
              </div>
              <div className={s.trendingList}>{trendingList}</div>
            </div>
          </Col>
          <Col sm={6}>
            <div className={s.helpfulNerds}>
              <div className={s.title}>
                <h2>Most Helpful Nerds</h2>
                <p>People who love to giving solution to other nerds problem</p>
              </div>
              <div className={s.nerdList}>{nerdList}</div>
            </div>
          </Col>
        </Row>
      </Grid>
    </section>
  );
}
