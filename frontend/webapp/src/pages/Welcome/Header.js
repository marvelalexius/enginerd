import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';

import { Grid } from '../../components/Layout';

import s from './header.module.css';

export default function Header() {
  return (
    <header className={cn(s.blockedDisplay, s.header)}>
      <Grid className={s.headerGrid}>
        <h1 className={s.logo}>
          <Link to="/">
            <i className="fa fa-laptop" /> enginerd.co
          </Link>
        </h1>
        <nav className={cn(s.navMenu, s.list)}>
          <ul>
            <li>
              <Link to="/explore">Explore</Link>
            </li>
            <li>
              <Link to="/ama">AMA</Link>
            </li>
            <li>
              <Link to="/event">Events</Link>
            </li>
          </ul>
        </nav>
      </Grid>
    </header>
  );
}
