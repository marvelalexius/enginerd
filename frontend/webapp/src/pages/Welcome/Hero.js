import React from 'react';
import cn from 'classnames';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { compose } from 'redux';

import { Grid, Row, Col } from '../../components/Layout';
import { actions } from '../../ducks';

import s from './hero.module.css';

class Hero extends React.Component {
  state = {
    form: {
      username: '',
      password: ''
    }
  };

  handleClickGoogle = event => {
    event.preventDefault();
    window.location.href = `${window.Enginerd.hostname}/authorize/google`;
  };

  handleClickTwitter = event => {
    event.preventDefault();
    window.location.href = `${window.Enginerd.hostname}/authorize/twitter`;
  };

  handleClickGithub = event => {
    event.preventDefault();
    window.location.href = `${window.Enginerd.hostname}/authorize/github`;
  };

  handleChange = key => ({ target }) => {
    this.setState({
      ...this.state,
      form: { ...this.state.form, [key]: target.value }
    });
  };

  handleLogin = () => {
    this.props.doLogin(this.state.form);
  };

  render() {
    return (
      <section className={s.heroContainer}>
        <Grid>
          <Row>
            <Col sm={7}>
              <main className={s.heroContent}>
                <div>
                  <h2>a home for &lt;Engineer /&gt;</h2>
                  <h3>Ask. Learn. Share</h3>
                  <div className={s.search}>
                    <label htmlFor="search">
                      only walking around all communities?
                    </label>
                    <input
                      type="search"
                      id="search"
                      name="topic"
                      placeholder="Try ReactJS, Django, Rails, Hadoop..."
                    />
                  </div>
                  <div className={s.createNew}>
                    <p>...or ask a question to communities?</p>
                    <button className={s.getStarted}>Get Started</button>
                  </div>
                </div>
              </main>
            </Col>
            <Col sm={5}>
              <nav className={s.loginWith}>
                <ul>
                  <li>
                    <input
                      type="text"
                      onChange={this.handleChange('username')}
                      placeholder="Username"
                      name="username"
                    />
                    <input
                      type="password"
                      onChange={this.handleChange('password')}
                      placeholder="Password"
                    />
                    <button
                      className={cn(s.loginBtn, s.jwt)}
                      onClick={this.handleLogin}
                    >
                      <span>Signin</span>
                    </button>
                  </li>
                  <li>
                    <p>or</p>
                  </li>
                  <li>
                    <button
                      className={cn(s.loginBtn, s.google)}
                      onClick={this.handleClickGoogle}
                    >
                      <i className="fa fa-google" />
                      <span>Signin with Google</span>
                    </button>
                  </li>
                  <li>
                    <button
                      className={cn(s.loginBtn, s.twitter)}
                      onClick={this.handleClickTwitter}
                    >
                      <i className="fa fa-twitter" />
                      <span>Signin with Twitter</span>
                    </button>
                  </li>
                  <li>
                    <button
                      className={cn(s.loginBtn, s.github)}
                      onClick={this.handleClickGithub}
                    >
                      <i className="fa fa-github" />
                      <span>Signin with Github</span>
                    </button>
                  </li>
                </ul>
              </nav>
            </Col>
          </Row>
        </Grid>
      </section>
    );
  }
}

const mapStateToProps = () => ({
  sate: ''
});

const mapDispatchToProps = { doLogin: actions.doLogin };

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Hero);
