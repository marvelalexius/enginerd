import React from 'react';
import { Helmet } from 'react-helmet';
import { withCookies, Cookies } from 'react-cookie';
import { compose } from 'redux';

import Footer from '../../components/Footer';

import Header from './Header';
import Hero from './Hero';
import Explore from './Explore';
import Trending from './Trending';

function Welcome(props) {
  return (
    <div>
      <Header />
      <Hero />
      <Explore />
      <Trending />
      <Footer />
      <Helmet>
        <body className="welcome-page" />
      </Helmet>
    </div>
  );
}

export default compose(withCookies)(Welcome);
