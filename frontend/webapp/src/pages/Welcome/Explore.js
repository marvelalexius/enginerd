import React, { Component } from 'react';
import cn from 'classnames';

import { Grid, Row, Col } from '../../components/Layout';
import ItemExplore from '../../components/ItemExplore';

import topics from './mock/topics.json';
import s from './explore.module.css';

export default class Explore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'featured'
    };
  }

  handleSwitchTab = name => {
    this.setState({ activeTab: name });
  };

  panelList = () => {
    return topics.map((topic, i) => <ItemExplore key={i} {...topic} />);
  };

  render() {
    const { activeTab } = this.state;

    return (
      <section className={s.exploreContainer}>
        <Grid>
          <div className={s.exploreHeader}>
            <h2>Explore the community</h2>
            <p>Find a community for you!</p>
          </div>
        </Grid>
        <Grid className={s.exploreContent}>
          <div>
            <nav className={s.tab}>
              <ul>
                <li className={cn(activeTab === 'featured' && s.active)}>
                  <button onClick={() => this.handleSwitchTab('featured')}>
                    Featured
                  </button>
                </li>
                <li className={cn(activeTab === 'top-active' && s.active)}>
                  <button onClick={() => this.handleSwitchTab('top-active')}>
                    Top Active
                  </button>
                </li>
                <li className={cn(activeTab === 'most-followed' && s.active)}>
                  <button onClick={() => this.handleSwitchTab('most-followed')}>
                    Most Followed
                  </button>
                </li>
              </ul>
            </nav>
            <div className={s.panel}>{this.panelList()}</div>
          </div>
        </Grid>
      </section>
    );
  }
}
