# ENGINERD

a forum that made for human™

# Our philosophy

Do whatever feature you like, faster deliver means faster feedback from user.
Better done than perfect. Forget about fancy UI, just deliver it.

> Think big, start small, move fast, break things

# Instalation

Requirements to develop enginerd app.

- Install [https://www.virtualbox.org/wiki/Downloads](VirtualBox)
- Go to enginerd root folder and run `chmod +x onboarding && onboarding`
- To start server, run `enginerd build` first, then `enginerd up`
